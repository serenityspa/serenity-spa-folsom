Serenity Spa | Soul Yoga is a temple with the intent to provide healers, teachers, light workers, and community a place to gather, connect, & transform. Visit us at our Roseville or Folsom location.

Address: 350 Palladio Pkwy, #1975, Folsom, CA 95630, USA

Phone: 916-542-7363
